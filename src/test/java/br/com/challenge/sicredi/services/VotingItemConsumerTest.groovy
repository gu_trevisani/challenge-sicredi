package br.com.challenge.sicredi.services

import br.com.challenge.sicredi.dtos.VoteRequest
import br.com.challenge.sicredi.dtos.VotingItemRequest
import br.com.challenge.sicredi.dtos.VotingSessionRequest
import br.com.challenge.sicredi.enums.Choice
import br.com.challenge.sicredi.models.VotingItem
import org.junit.Test
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalTime

class VotingItemConsumerTest extends Specification {

    VotingItemService votingItemService = Mock()

    @Unroll
    @Test
    def "should call create method with incoming request parameter"() {
        given:
        VotingItemConsumer votingItemConsumer = new VotingItemConsumer(votingItemService)
        VotingItemRequest votingItemRequest = new VotingItemRequest("Any String")
        when:
        votingItemConsumer.consumeFromVotingItemQueue(votingItemRequest)
        then:
        1 * votingItemService.createVotingItem(votingItemRequest.getVotingItemName())
        0 * _
    }

    @Unroll
    @Test
    def "should call open session method with incoming request parameter"() {
        given:
        VotingItemConsumer votingItemConsumer = new VotingItemConsumer(votingItemService)
        VotingSessionRequest votingSessionRequest = new VotingSessionRequest(1, LocalTime.of(1, 30))
        when:
        votingItemConsumer.consumeFromVotingSessionQueue(votingSessionRequest)
        then:
        1 * votingItemService.startVotingSession(votingSessionRequest)
        0 * _
    }

    @Unroll
    @Test
    def "should call vote method with incoming request parameter"() {
        given:
        VotingItemConsumer votingItemConsumer = new VotingItemConsumer(votingItemService)
        VoteRequest voteRequest = new VoteRequest(1, "123", Choice.YES)
        when:
        votingItemConsumer.consumeFromVoteQueue(voteRequest)
        then:
        1 * votingItemService.processVote(voteRequest)
        0 * _
    }

    @Unroll
    @Test
    def "should call evaluate result method with incoming request parameter"() {
        given:
        VotingItemConsumer votingItemConsumer = new VotingItemConsumer(votingItemService)
        List<VotingItem> votingItemList = [new VotingItem(), new VotingItem()]
        when:
        votingItemConsumer.consumeFromVoteResultQueue(votingItemList)
        then:
        2 * votingItemService.processVotingItemResult(_ as VotingItem)
        0 * _
    }

}
