package br.com.challenge.sicredi.services


import br.com.challenge.sicredi.dtos.VotingSessionRequest
import br.com.challenge.sicredi.enums.Choice
import br.com.challenge.sicredi.enums.Result
import br.com.challenge.sicredi.models.Vote
import br.com.challenge.sicredi.models.VotingItem
import br.com.challenge.sicredi.models.VotingResult
import br.com.challenge.sicredi.models.VotingSession
import br.com.challenge.sicredi.repositories.VoteRepo
import br.com.challenge.sicredi.repositories.VotingItemRepo
import br.com.challenge.sicredi.repositories.VotingResultRepo
import org.junit.Test
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime
import java.time.LocalTime

class VotingItemServiceTest extends Specification {

    VotingItemRepo votingItemRepo = Mock()
    VoteRepo voteRepo = Mock()
    VotingResultRepo votingResultRepo = Mock()

    @Unroll
    @Test
    def "should create voting item"() {
        given:
        VotingItemService votingItemService = new VotingItemService(votingItemRepo, voteRepo, votingResultRepo)
        when:
        VotingItem result = votingItemService.createVotingItem(expectedName)
        then:
        1 * votingItemRepo.save(_ as VotingItem) >> new VotingItem(expectedName, null)
        0 * _
        result.getName() == expectedName
        where:
        expectedName << ["Change Logo Main Color to Blue", "New President"]
    }

    @Unroll
    @Test
    def "should start voting session"() {
        given:
        VotingItemService votingItemService = new VotingItemService(votingItemRepo, voteRepo, votingResultRepo)
        when:
        votingItemService.startVotingSession(votingSessionRequest)
        then:
        1 * votingItemRepo.findById(votingSessionRequest.getVotingItemId()) >> Optional.of(new VotingItem("any name", new VotingSession(LocalTime.of(0, 1)), new ArrayList<>(), null))
        1 * votingItemRepo.save(_ as VotingItem) >> new VotingItem(_ as String, new VotingSession(LocalTime.of(0, 1)))
        0 * _
        where:
        votingSessionRequest << [new VotingSessionRequest(1, LocalTime.of(0, 30)), new VotingSessionRequest(1, null)]
    }

    @Unroll
    @Test
    def "should process voting result"() {
        given:
        VotingItemService votingItemService = new VotingItemService(votingItemRepo, voteRepo, votingResultRepo)
        when:
        VotingResult result = votingItemService.getResult(1)
        then:
        1 * votingItemRepo.findById(_ as Long) >> Optional.of(new VotingItem("any name", new VotingSession(LocalTime.of(0, 1), LocalDateTime.now().minusMinutes(1)), voteList, null))
        1 * votingResultRepo.save(_ as VotingResult)
        1 * votingItemRepo.save(_ as VotingItem)
        0 * _
        result.getResult() == expectedResult
        where:
        voteList                                                                               | expectedResult
        [new Vote("123", Choice.YES), new Vote("456", Choice.YES), new Vote("789", Choice.NO)] | Result.APROOVED
        [new Vote("123", Choice.YES), new Vote("456", Choice.NO), new Vote("789", Choice.NO)]  | Result.UNAPROOVED
        [new Vote("123", Choice.YES), new Vote("456", Choice.NO)]                              | Result.TIE
    }

    @Unroll
    @Test
    def "should throw exception when session still (or never) open"() {
        given:
        VotingItemService votingItemService = new VotingItemService(votingItemRepo, voteRepo, votingResultRepo)
        when:
        votingItemService.getResult(1)
        then:
        1 * votingItemRepo.findById(_ as Long) >> Optional.of(new VotingItem(1, "any name", session, [], null))
        0 * _
        def e = thrown(Exception)
        e.getMessage() == expectedExceptionMessage
        where:
        session                                                                   | expectedExceptionMessage
        new VotingSession(LocalTime.of(0, 1), null)                               | "Session of voting item with id \"1\" was not opened yet."
        new VotingSession(LocalTime.of(0, 1), LocalDateTime.now().plusMinutes(1)) | "Session of voting item with id \"1\" is still open."
    }

    @Unroll
    @Test
    def "should evaluate the corret result for the givem voting item"() {
        given:
        VotingItemService votingItemService = new VotingItemService(votingItemRepo, voteRepo, votingResultRepo)
        when:
        VotingResult result = votingItemService.processVotingItemResult(givenVotingItem)
        then:
        1 * votingResultRepo.save(_ as VotingResult)
        1 * votingItemRepo.save(_ as VotingItem)
        0 * _
        result.getResult() == expectedResult

        where:
        givenVotingItem                                                                                                                | expectedResult
        new VotingItem(null, null, null, [new Vote("123", Choice.YES), new Vote("456", Choice.YES), new Vote("789", Choice.NO)], null) | Result.APROOVED
        new VotingItem(null, null, null, [new Vote("123", Choice.YES), new Vote("456", Choice.NO), new Vote("789", Choice.NO)], null)  | Result.UNAPROOVED
        new VotingItem(null, null, null, [new Vote("123", Choice.YES), new Vote("456", Choice.NO)], null)                              | Result.TIE
    }

}
