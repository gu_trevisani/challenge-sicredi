package br.com.challenge.sicredi.controllers

import br.com.challenge.sicredi.configuration.RabbitMqConfig
import br.com.challenge.sicredi.dtos.VoteRequest
import br.com.challenge.sicredi.dtos.VoteResultRequest
import br.com.challenge.sicredi.dtos.VotingItemRequest
import br.com.challenge.sicredi.dtos.VotingSessionRequest
import br.com.challenge.sicredi.enums.Choice
import br.com.challenge.sicredi.enums.Result
import br.com.challenge.sicredi.models.VotingResult
import br.com.challenge.sicredi.services.VotingItemService
import org.junit.Test
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.http.ResponseEntity
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalTime

class VotingItemControllerTest extends Specification {

    VotingItemService votingItemService = Mock()
    private RabbitTemplate rabbitTemplate = Mock()

    @Unroll
    @Test
    def "should send request to voting item queue"() {
        given:
        VotingItemController votingItemController = new VotingItemController(votingItemService, rabbitTemplate)
        ResponseEntity response
        when:
        response = votingItemController.createVotingItem(votingItemRequest)
        then:
        1 * rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTING_ITEM_KEY, votingItemRequest)
        0 * _
        response.body == "Create Voting Item with name \"" + votingItemRequest.getVotingItemName() + "\" request successfully received."
        where:
        votingItemRequest << [new VotingItemRequest("Any Name"), new VotingItemRequest("Other_Name")]
    }

    @Unroll
    @Test
    def "should send request to voting session queue"() {
        given:
        VotingItemController votingItemController = new VotingItemController(votingItemService, rabbitTemplate)
        ResponseEntity response
        when:
        response = votingItemController.startVotingSession(votingSessionRequest)
        then:
        1 * rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTING_SESSION_KEY, votingSessionRequest)
        0 * _
        response.body == "Open session for voting item with id \"" + votingSessionRequest.getVotingItemId() + "\" request successfully received."
        where:
        votingSessionRequest << [new VotingSessionRequest(1, LocalTime.of(0, 30)), new VotingSessionRequest(1, null)]
    }

    @Unroll
    @Test
    def "should send request to vote queue"() {
        given:
        VotingItemController votingItemController = new VotingItemController(votingItemService, rabbitTemplate)
        ResponseEntity response
        when:
        response = votingItemController.vote(voteRequest)
        then:
        1 * rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTE_KEY, voteRequest)
        0 * _
        response.body == "Vote request successfully received."
        where:
        voteRequest << [new VoteRequest(1, "123", Choice.YES), new VoteRequest(1, "321", Choice.NO)]
    }

    @Test
    def "should return the voting item result"() {
        given:
        VotingItemController votingItemController = new VotingItemController(votingItemService, rabbitTemplate)
        VoteResultRequest voteResultRequest = new VoteResultRequest(1)
        VotingResult result = new VotingResult(1, 2, 3, Result.UNAPROOVED)
        ResponseEntity response
        when:
        response = votingItemController.result(voteResultRequest)
        then:
        1 * votingItemService.getResult(voteResultRequest.getVotingItemId()) >> result
        0 * _
        response.body == result
    }
}
