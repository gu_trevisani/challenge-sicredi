package br.com.challenge.sicredi.models;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@NoArgsConstructor
public class VotingSession {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "SESSION_DURATION")
    private LocalTime sessionDuration;
    @Column(name = "SESSION_END_DATE_TIME")
    private LocalDateTime sessionEndDateTime;

    public VotingSession(LocalTime sessionDuration, LocalDateTime sessionEndDateTime) {
        this.sessionDuration = sessionDuration;
        this.sessionEndDateTime = sessionEndDateTime;
    }

    public VotingSession(LocalTime sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(LocalTime sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public LocalDateTime getSessionEndDateTime() {
        return sessionEndDateTime;
    }

    public void setSessionEndDateTime(LocalDateTime sessionEndDateTime) {
        this.sessionEndDateTime = sessionEndDateTime;
    }
}
