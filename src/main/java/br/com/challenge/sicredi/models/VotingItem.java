package br.com.challenge.sicredi.models;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@NoArgsConstructor
public class VotingItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @NotBlank
    @Column(name = "NAME")
    private String name;
    @OneToOne(targetEntity = VotingSession.class, cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "VOTING_SESSION_ID", referencedColumnName = "ID")
    private VotingSession votingSession;
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "VOTING_ITEM_ID")
    private List<Vote> voteList;
    @OneToOne(targetEntity = VotingResult.class, cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "VOTING_RESULT_ID", referencedColumnName = "ID")
    private VotingResult votingResult;

    public VotingItem(Long id, @NotBlank String name, VotingSession votingSession, List<Vote> voteList, VotingResult votingResult) {
        this(name, votingSession, voteList, votingResult);
        this.id = id;
    }

    public VotingItem(@NotBlank String name, VotingSession votingSession, List<Vote> voteList, VotingResult votingResult) {
        this.name = name;
        this.votingSession = votingSession;
        this.voteList = voteList;
        this.votingResult = votingResult;
    }

    public VotingItem(String votingItemName, VotingSession votingSession) {
        this.name = votingItemName;
        this.votingSession = votingSession;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VotingSession getVotingSession() {
        return votingSession;
    }

    public void setVotingSession(VotingSession votingSession) {
        this.votingSession = votingSession;
    }

    public List<Vote> getVoteList() {
        return voteList;
    }

    public void setVoteList(List<Vote> voteList) {
        this.voteList = voteList;
    }

    public VotingResult getVotingResult() {
        return votingResult;
    }

    public void setVotingResult(VotingResult votingResult) {
        this.votingResult = votingResult;
    }
}
