package br.com.challenge.sicredi.models;

import br.com.challenge.sicredi.enums.Choice;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
public class Vote {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ASSOCIATED_ID")
    private String associatedId;
    @Column(name = "CHOICE")
    @Enumerated(EnumType.STRING)
    private Choice choice;

    public Vote(String associatedId, Choice choice) {
        this.associatedId = associatedId;
        this.choice = choice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssociatedId() {
        return associatedId;
    }

    public void setAssociatedId(String associatedId) {
        this.associatedId = associatedId;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }
}
