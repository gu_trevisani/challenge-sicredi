package br.com.challenge.sicredi.models;

import br.com.challenge.sicredi.enums.Result;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
public class VotingResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    @JsonIgnore
    private Long id;
    @Column(name = "POSITIVE_VOTES_AMOUNT")
    private Integer positiveVotesAmount;
    @Column(name = "NEGATIVE_VOTES_AMOUNT")
    private Integer negativeVotesAmount;
    @Column(name = "result")
    @Enumerated(EnumType.STRING)
    private Result result;

    public VotingResult(Long id, Integer positiveVotesAmount, Integer negativeVotesAmount, Result result) {
        this.id = id;
        this.positiveVotesAmount = positiveVotesAmount;
        this.negativeVotesAmount = negativeVotesAmount;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPositiveVotesAmount() {
        return positiveVotesAmount;
    }

    public void setPositiveVotesAmount(Integer positiveVotesAmount) {
        this.positiveVotesAmount = positiveVotesAmount;
    }

    public Integer getNegativeVotesAmount() {
        return negativeVotesAmount;
    }

    public void setNegativeVotesAmount(Integer negativeVotesAmount) {
        this.negativeVotesAmount = negativeVotesAmount;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
