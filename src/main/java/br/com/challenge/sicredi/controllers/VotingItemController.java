package br.com.challenge.sicredi.controllers;

import br.com.challenge.sicredi.configuration.RabbitMqConfig;
import br.com.challenge.sicredi.dtos.VoteRequest;
import br.com.challenge.sicredi.dtos.VoteResultRequest;
import br.com.challenge.sicredi.dtos.VotingItemRequest;
import br.com.challenge.sicredi.dtos.VotingSessionRequest;
import br.com.challenge.sicredi.models.VotingResult;
import br.com.challenge.sicredi.services.VotingItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/v1/votingItem")
@Api(value = "VotingItem")
@Slf4j
public class VotingItemController {

    private VotingItemService votingItemService;
    private RabbitTemplate rabbitTemplate;

    @Inject
    public VotingItemController(VotingItemService votingItemService, RabbitTemplate rabbitTemplate) {
        this.votingItemService = votingItemService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @ApiOperation(value = "Create a Voting Item")
    @PostMapping(value = "/create")
    public ResponseEntity<String> createVotingItem(@RequestBody @Valid VotingItemRequest votingItemRequest) {
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTING_ITEM_KEY, votingItemRequest);
        log.debug("Create Voting Item with name \"" + votingItemRequest.getVotingItemName() + "\" request sent to queue");
        return ResponseEntity.ok("Create Voting Item with name \"" + votingItemRequest.getVotingItemName() + "\" request successfully received.");
    }

    @ApiOperation(value = "Start a voting item session")
    @PostMapping(value = "/startVotingSession")
    public ResponseEntity<String> startVotingSession(@RequestBody @Valid VotingSessionRequest votingSessionRequest) {
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTING_SESSION_KEY, votingSessionRequest);
        log.debug("Open session for voting item with id \"" + votingSessionRequest.getVotingItemId() + "\" sent to queue");
        return ResponseEntity.ok("Open session for voting item with id \"" + votingSessionRequest.getVotingItemId() + "\" request successfully received.");
    }

    @ApiOperation(value = "Perform a vote to an explicit voting item")
    @PostMapping(value = "/vote")
    public ResponseEntity<String> vote(@RequestBody @Valid VoteRequest voteRequest) {
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTE_KEY, voteRequest);
        log.debug("Vote request sent to queue");
        return ResponseEntity.ok("Vote request successfully received.");
    }

    @ApiOperation(value = "Returns the votes result of a voting item")
    @GetMapping(value = "/result")
    public ResponseEntity<VotingResult> result(@Valid VoteResultRequest voteResultRequest) throws Exception {
        log.debug("Fetching votes result for voting item with id \"" + voteResultRequest.getVotingItemId() + "\"");
        return ResponseEntity.ok(votingItemService.getResult(voteResultRequest.getVotingItemId()));
    }

}
