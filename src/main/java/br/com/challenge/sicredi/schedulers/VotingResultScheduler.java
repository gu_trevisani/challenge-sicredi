package br.com.challenge.sicredi.schedulers;

import br.com.challenge.sicredi.configuration.RabbitMqConfig;
import br.com.challenge.sicredi.models.VotingItem;
import br.com.challenge.sicredi.repositories.VotingItemRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class VotingResultScheduler {

    VotingItemRepo votingItemRepo;
    RabbitTemplate rabbitTemplate;

    public VotingResultScheduler(VotingItemRepo votingItemRepo, RabbitTemplate rabbitTemplate) {
        this.votingItemRepo = votingItemRepo;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(cron = "1 * * * * *")
    public void evaluateVotingResults() {
        List<VotingItem> votingItemList = votingItemRepo.findAllUnprocededResultsVotingItems();
        if (votingItemList.size() > 1) {
            rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.VOTE_RESULT_KEY, votingItemList);
            log.debug("Evaluating vote results of ended sessions");
        }

    }

}
