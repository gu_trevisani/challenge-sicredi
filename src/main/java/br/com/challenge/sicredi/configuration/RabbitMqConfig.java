package br.com.challenge.sicredi.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RabbitMqConfig {

    public static final String VOTING_ITEM_QUEUE = "voting_item_request_queue";
    public static final String VOTING_SESSION_QUEUE = "voting_session_request_queue";
    public static final String VOTE_QUEUE = "vote_request_queue";
    public static final String VOTE_RESULT_QUEUE = "vote_result_queue";

    public static final String EXCHANGE = "voting_exchange";

    public static final String VOTING_ITEM_KEY = "voting_item_routingKey";
    public static final String VOTING_SESSION_KEY = "voting_session_routingKey";
    public static final String VOTE_KEY = "vote_routingKey";
    public static final String VOTE_RESULT_KEY = "vote_result_routingKey";

    @Bean
    @Primary
    public Queue votingItemQueue() {
        return new Queue(VOTING_ITEM_QUEUE);
    }

    @Bean
    public Queue votingSessionQueue() {
        return new Queue(VOTING_SESSION_QUEUE);
    }

    @Bean
    public Queue voteQueue() {
        return new Queue(VOTE_QUEUE);
    }

    @Bean
    public Queue voteResultQueue() {
        return new Queue(VOTE_RESULT_QUEUE);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    Binding votingItemBinding(TopicExchange exchange) {
        return BindingBuilder.bind(votingItemQueue()).to(exchange).with(VOTING_ITEM_KEY);
    }

    @Bean
    Binding votingSessionBinding(TopicExchange exchange) {
        return BindingBuilder.bind(votingSessionQueue()).to(exchange).with(VOTING_SESSION_KEY);
    }

    @Bean
    Binding voteBinding(TopicExchange exchange) {
        return BindingBuilder.bind(voteQueue()).to(exchange).with(VOTE_KEY);
    }

    @Bean
    Binding voteResultBinding(TopicExchange exchange) {
        return BindingBuilder.bind(voteResultQueue()).to(exchange).with(VOTE_RESULT_KEY);
    }

    @Bean
    public MessageConverter converter() {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}
