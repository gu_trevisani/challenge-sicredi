package br.com.challenge.sicredi.repositories;

import br.com.challenge.sicredi.models.VotingItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotingItemRepo extends JpaRepository<VotingItem, Long> {

    @Query(value = "SELECT * FROM voting_item v " +
            "LEFT JOIN voting_session s ON s.id = v.voting_session_id " +
            "WHERE s.session_end_date_time IS NOT NULL " +
            "AND s.session_end_date_time  < sysdate " +
            "AND v.voting_result_id IS NULL", nativeQuery = true)
    List<VotingItem> findAllUnprocededResultsVotingItems();
}
