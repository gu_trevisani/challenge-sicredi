package br.com.challenge.sicredi.repositories;

import br.com.challenge.sicredi.models.VotingResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotingResultRepo extends JpaRepository<VotingResult, Long> {
}
