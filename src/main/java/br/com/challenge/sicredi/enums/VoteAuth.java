package br.com.challenge.sicredi.enums;

public enum VoteAuth {
    ABLE_TO_VOTE,
    UNABLE_TO_VOTE
}
