package br.com.challenge.sicredi.dtos;

import br.com.challenge.sicredi.enums.Choice;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
public class VoteRequest {

    @NotNull
    private Long votingItemId;
    @NotBlank
    private String associatedId;
    @NotNull
    private Choice choice;

    public VoteRequest(@NotNull Long votingItemId, @NotBlank String associatedId, @NotNull Choice choice) {
        this.votingItemId = votingItemId;
        this.associatedId = associatedId;
        this.choice = choice;
    }

    public Long getVotingItemId() {
        return votingItemId;
    }

    public void setVotingItemId(Long votingItemId) {
        this.votingItemId = votingItemId;
    }

    public String getAssociatedId() {
        return associatedId;
    }

    public void setAssociatedId(String associatedId) {
        this.associatedId = associatedId;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }
}
