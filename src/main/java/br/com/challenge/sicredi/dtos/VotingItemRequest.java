package br.com.challenge.sicredi.dtos;

import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
public class VotingItemRequest {

    @NotBlank
    private String votingItemName;

    public VotingItemRequest(@NotBlank String votingItemName) {
        this.votingItemName = votingItemName;
    }

    public String getVotingItemName() {
        return votingItemName;
    }

    public void setVotingItemName(String votingItemName) {
        this.votingItemName = votingItemName;
    }
}
