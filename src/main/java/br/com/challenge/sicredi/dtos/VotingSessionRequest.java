package br.com.challenge.sicredi.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@NoArgsConstructor
public class VotingSessionRequest {

    @NotNull
    private Long votingItemId;
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime duration;

    public VotingSessionRequest(@NotNull Long votingItemId, LocalTime duration) {
        this.votingItemId = votingItemId;
        this.duration = duration;
    }

    public Long getVotingItemId() {
        return votingItemId;
    }

    public void setVotingItemId(Long votingItemId) {
        this.votingItemId = votingItemId;
    }

    public LocalTime getDuration() {
        return duration;
    }

    public void setDuration(LocalTime duration) {
        this.duration = duration;
    }


}
