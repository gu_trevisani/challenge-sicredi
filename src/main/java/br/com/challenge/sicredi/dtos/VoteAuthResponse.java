package br.com.challenge.sicredi.dtos;

import br.com.challenge.sicredi.enums.VoteAuth;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class VoteAuthResponse {

    private VoteAuth status;

    public VoteAuth getStatus() {
        return status;
    }

    public void setStatus(VoteAuth status) {
        this.status = status;
    }
}
