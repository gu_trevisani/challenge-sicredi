package br.com.challenge.sicredi.dtos;

import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
public class VoteResultRequest {

    @NotNull
    private Long votingItemId;

    public VoteResultRequest(@NotNull Long votingItemId) {
        this.votingItemId = votingItemId;
    }

    public Long getVotingItemId() {
        return votingItemId;
    }

    public void setVotingItemId(Long votingItemId) {
        this.votingItemId = votingItemId;
    }

}
