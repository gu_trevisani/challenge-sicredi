package br.com.challenge.sicredi.services;


import br.com.challenge.sicredi.configuration.RabbitMqConfig;
import br.com.challenge.sicredi.dtos.VoteRequest;
import br.com.challenge.sicredi.dtos.VotingItemRequest;
import br.com.challenge.sicredi.dtos.VotingSessionRequest;
import br.com.challenge.sicredi.models.VotingItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
@Slf4j
public class VotingItemConsumer {

    private VotingItemService votingItemService;

    @Inject
    public VotingItemConsumer(VotingItemService votingItemService) {
        this.votingItemService = votingItemService;
    }

    @RabbitListener(queues = RabbitMqConfig.VOTING_ITEM_QUEUE)
    public void consumeFromVotingItemQueue(VotingItemRequest votingItemRequest) {
        try {
            votingItemService.createVotingItem(votingItemRequest.getVotingItemName());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @RabbitListener(queues = RabbitMqConfig.VOTING_SESSION_QUEUE)
    public void consumeFromVotingSessionQueue(VotingSessionRequest votingSessionRequest) {
        try {
            votingItemService.startVotingSession(votingSessionRequest);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @RabbitListener(queues = RabbitMqConfig.VOTE_QUEUE)
    public void consumeFromVoteQueue(VoteRequest voteRequest) throws Exception {
        try {
            votingItemService.processVote(voteRequest);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @RabbitListener(queues = RabbitMqConfig.VOTE_RESULT_QUEUE)
    public void consumeFromVoteResultQueue(List<VotingItem> votingItemList) throws Exception {
        try {
            for (VotingItem votingItem : votingItemList) {
                votingItemService.processVotingItemResult(votingItem);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
