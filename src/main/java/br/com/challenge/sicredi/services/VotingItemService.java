package br.com.challenge.sicredi.services;

import br.com.challenge.sicredi.dtos.VoteAuthResponse;
import br.com.challenge.sicredi.dtos.VoteRequest;
import br.com.challenge.sicredi.dtos.VotingSessionRequest;
import br.com.challenge.sicredi.enums.Choice;
import br.com.challenge.sicredi.enums.Result;
import br.com.challenge.sicredi.enums.VoteAuth;
import br.com.challenge.sicredi.models.Vote;
import br.com.challenge.sicredi.models.VotingItem;
import br.com.challenge.sicredi.models.VotingResult;
import br.com.challenge.sicredi.models.VotingSession;
import br.com.challenge.sicredi.repositories.VoteRepo;
import br.com.challenge.sicredi.repositories.VotingItemRepo;
import br.com.challenge.sicredi.repositories.VotingResultRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VotingItemService {

    private VotingItemRepo votingItemRepo;
    private VoteRepo voteRepo;
    private VotingResultRepo votingResultRepo;

    @Inject
    public VotingItemService(VotingItemRepo votingItemRepo, VoteRepo voteRepo, VotingResultRepo votingResultRepo) {
        this.votingItemRepo = votingItemRepo;
        this.voteRepo = voteRepo;
        this.votingResultRepo = votingResultRepo;
    }

    public VotingItem createVotingItem(String votingItemName) {
        log.debug("Creating voting item with name \"" + votingItemName + "\"");
        VotingSession votingSession = new VotingSession(LocalTime.of(0, 1));
        VotingItem votingItem = new VotingItem(votingItemName, votingSession);
        return votingItemRepo.save(votingItem);
    }

    public VotingItem startVotingSession(VotingSessionRequest votingSessionRequest) {
        log.debug("Starting session for voting item with id \"" + votingSessionRequest.getVotingItemId() + "\"");
        VotingItem votingItem = findVotingItemById(votingSessionRequest.getVotingItemId());
        VotingSession votingSession = votingItem.getVotingSession();
        if (votingSessionRequest.getDuration() != null) {
            votingSession.setSessionDuration(votingSessionRequest.getDuration());
        }
        votingSession.setSessionEndDateTime(LocalDateTime.now().plusSeconds(votingSession.getSessionDuration().toSecondOfDay()));
        return votingItemRepo.save(votingItem);
    }

    public VotingItem processVote(VoteRequest voteRequest) throws Exception {
        log.debug("Processing vote for voting item with id \"" + voteRequest.getVotingItemId() + "\"");
        checkAbleToVote(voteRequest.getAssociatedId());
        VotingItem votingItem = findVotingItemById(voteRequest.getVotingItemId());
        checkSessionNotOpenOrAlreadyClosed(votingItem);
        List<Vote> voteList = votingItem.getVoteList() != null ? votingItem.getVoteList() : new ArrayList<>();
        checkAlreadyVoted(voteList, voteRequest);
        Vote vote = new Vote(voteRequest.getAssociatedId(), voteRequest.getChoice());
        voteList.add(vote);
        voteRepo.save(vote);
        votingItem.setVoteList(voteList);
        log.debug("Vote for voting item with id \"" + voteRequest.getVotingItemId() + "\" processed");
        return votingItemRepo.save(votingItem);
    }

    private void checkAbleToVote(String associatedId) throws Exception {
        log.debug("Checking if associated with id \"" + associatedId + "\" is valid and able to vote");
        RestTemplate restTemplate = new RestTemplate();
        VoteAuthResponse auth = restTemplate.getForObject("https://user-info.herokuapp.com/users/" + associatedId, VoteAuthResponse.class);
        String errorMessage;
        if (auth == null) {
            errorMessage = "CPF \"" + associatedId + "\" is invalid.";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
        if (auth.getStatus().equals(VoteAuth.UNABLE_TO_VOTE)) {
            errorMessage = "CPF \"" + associatedId + "\" is unable to vote.";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
    }

    private void checkSessionNotOpenOrAlreadyClosed(VotingItem votingItem) throws Exception {
        log.debug("Checking if session of voting item with id \"" + votingItem.getId() + "\" is not open or already closed");
        LocalDateTime sessionEndDateTime = votingItem.getVotingSession().getSessionEndDateTime();
        String errorMessage;
        if (sessionEndDateTime == null) {
            errorMessage = "Session for voting item with id \"" + votingItem.getId() + "\" is not open to vote yet.";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
        if (LocalDateTime.now().isAfter(sessionEndDateTime)) {
            errorMessage = "Session for voting item with id \"" + votingItem.getId() + "\" already closed at " + sessionEndDateTime + ".";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
    }

    private void checkAlreadyVoted(List<Vote> voteList, VoteRequest voteRequest) throws Exception {
        log.debug("Checking if associated with id \"" + voteRequest.getAssociatedId() + "\" already voted for the votin item with id \"" + voteRequest.getVotingItemId() + "\"");
        List<String> alreadyVotedAssociatedsIds = voteList.stream().map(Vote::getAssociatedId).collect(Collectors.toCollection(ArrayList::new));
        if (alreadyVotedAssociatedsIds.contains(voteRequest.getAssociatedId())) {
            String errorMessage = "Associated already voted for the voting item with id \"" + voteRequest.getVotingItemId() + "\".";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
    }

    public VotingResult getResult(Long votingItemId) throws Exception {
        VotingItem votingItem = findVotingItemById(votingItemId);
        checkSessionStillOrNeverOpened(votingItem);
        if (votingItem.getVotingResult() != null) {
            return votingItem.getVotingResult();
        }
        return processVotingItemResult(votingItem);
    }

    private void checkSessionStillOrNeverOpened(VotingItem votingItem) throws Exception {
        log.debug("Checking if session of voting item with id \"" + votingItem.getId() + "\" is still open or never opened");
        String errorMessage;
        if (votingItem.getVotingSession().getSessionEndDateTime() == null) {
            errorMessage = "Session of voting item with id \"" + votingItem.getId() + "\" was not opened yet.";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
        if (votingItem.getVotingSession().getSessionEndDateTime().isAfter(LocalDateTime.now())) {
            errorMessage = "Session of voting item with id \"" + votingItem.getId() + "\" is still open.";
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }
    }

    public VotingResult processVotingItemResult(VotingItem votingItem) {
        log.debug("Processing vote result for the voting item with id \"" + votingItem.getId() + "\"");
        VotingResult votingResult = new VotingResult();
        votingResult.setPositiveVotesAmount(countVotesByChoice(votingItem.getVoteList(), Choice.YES));
        votingResult.setNegativeVotesAmount(countVotesByChoice(votingItem.getVoteList(), Choice.NO));
        if (votingResult.getPositiveVotesAmount() > votingResult.getNegativeVotesAmount()) {
            votingResult.setResult(Result.APROOVED);
        } else if (votingResult.getPositiveVotesAmount() == votingResult.getNegativeVotesAmount()) {
            votingResult.setResult(Result.TIE);
        } else {
            votingResult.setResult(Result.UNAPROOVED);
        }
        votingItem.setVotingResult(votingResult);
        votingResultRepo.save(votingResult);
        votingItemRepo.save(votingItem);
        return votingResult;
    }

    private VotingItem findVotingItemById(Long votingItemId) {
        return votingItemRepo.findById(votingItemId).orElseThrow(() -> new EntityNotFoundException("Voting item not found for id \"" + votingItemId + "\"."));
    }

    private Integer countVotesByChoice(List<Vote> voteList, Choice choice) {
        return voteList.stream().filter(vote -> choice.equals(vote.getChoice())).collect(Collectors.toCollection(ArrayList::new)).size();
    }

}
