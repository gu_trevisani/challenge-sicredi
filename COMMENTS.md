#**Comentários**

### Como o projeto foi iniciado:
1) Criação do projeto usando spring initializer.
2) Adição de dependências que faltaram.
3) Implementação do código de forma síncrona sem utilização do RabbitMQ.
4) Utilização do Postman para executar as requests.
5) Implementação do swagger.
6) Implementação do RabbitMQ (publicação e consumo).
7) Realização de testes unitários
8) Configuração do Dockerfile e docker-compose

#**Ideias não implementadas**
Gostaria de ter implementado um scheduler para buscar as pautas com sessões finalizadas e sem resultado. 
Assim iria colocar em uma fila para processar e salvar o resultado automaticamente

#**Como rodar o projeto**
- Clonar o projeto do repositório fornecido.
- Acessar a pasta do repositório.
- Buildar a imagem da aplicação através do comando _docker build -t sicredi:1.0 ._
- Subir o docker compose através do comando _docker-compose up -d_
- Acompanhar os logs através do comando _docker-compose logs -f_, para garantir que aplicação já subiu
- Quando a aplicação já estiver rodando, apertar as teclas _CTRL + C_
- Abrir o browser e ir para a url "http://localhost:8080/h2-console"
- Preencher os campos com os dados fornecidos abaixo:
###### SavedSettings: Generic H@ (Embedded)
###### SettingName: Generic H@ (Embedded)
###### Driver Class: org.h2.Driver
###### JDBC URL: jdbc:h2:file:./data/db;Mode=Oracle;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE
###### User Name: 
###### Password: 
######***Não preencher o user name e password**
- Clicar em "connect".
- Executar as querys abaixo para buscar todos os dados referentes a aplicação:
 `
 SELECT * FROM VOTING_ITEM ;
 SELECT * FROM VOTING_SESSION ;
 SELECT * FROM VOTING_RESULT ;
 SELECT * FROM VOTE ; 
 `
DONE.



 

