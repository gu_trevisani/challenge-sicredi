FROM maven:3-openjdk-8

COPY . /usr/src/myapp

WORKDIR /usr/src/myapp

RUN  mvn clean install 

CMD ["mvn", "spring-boot:run"]